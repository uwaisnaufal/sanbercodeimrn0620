// Soal Nomor 1
console.log("\n Soal Nomor 1 \n")

function teriak(){
    console.log("Halo Sanbers!")
}

teriak();

//Soal Nomor 2 
console.log("\n Soal Nomor 2 \n")

    //Function
function kalikan(x,y){

    kali = x*y
    return kali

}

    //Var
var num1 = 12
var num2 = 4

    //Hasil
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) 


//Soal Nomor 3
console.log("\n Soal Nomor 3 \n")
    
    //Function
function introduce(n, a, ad, h){

    biodata = "Nama Saya " + n + ", Umur saya " + a + " Tahun, Alamat saya di " + ad + ", dan saya punya hobbi yaitu " + h + "!"
    return biodata

}
    //Var
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
    //Hasil
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)