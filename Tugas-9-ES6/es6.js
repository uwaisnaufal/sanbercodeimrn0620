//Soal Nomor 1
console.log("\n Soal Nomor 1")
console.log("============= \n")

const golden = () => {
  console.log("this is golden!!")
}
 
golden()

//Soal Nomor 2
console.log("\n Soal Nomor 2")
console.log("============= \n")

const newFunction = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName: () => {
        console.log(firstName + " " + lastName) 
      }
    }
  }
   
  newFunction("William", "Imoh").fullName() 

//Soal Nomor 3
console.log("\n Soal Nomor 3")
console.log("============= \n")

  const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
  
  const {firstName, lastName, destination, occupation, spell} = newObject;

  console.log(firstName, lastName, destination, occupation)

//Soal Nomor 4
console.log("\n Soal Nomor 4")
console.log("============= \n")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

console.log(combined)

//Soal Nomor 5
console.log("\n Soal Nomor 5")
console.log("============= \n")

const planet = "earth"
const view = "glass"
var before =    `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}do eiusmod tempor` + 
                `incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 

console.log(before) 
