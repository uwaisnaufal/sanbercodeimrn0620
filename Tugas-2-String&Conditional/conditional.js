//Soal If-Else
console.log("\n Soal If-Else \n");

console.log("Mari Selamatkan Desa dari kekejaman Werewolf! Cari Werewolf yang bersembunyi! Atau, Habisi warga dengan taring mu! \n");

    //Data Pemain
var namaKita = "Uwais";
var peranKita = "Werewolf";

    //Pengkondisian
if (namaKita) {
    
    console.log("Selamat datang di Dunia Werewolf, " + namaKita + "!");
    
    
    if (peranKita == "Werewolf") {

        console.log("Halo " + peranKita + " " + namaKita + "! Kamu akan memakan mangsa setiap malam!");
    }

        else if (peranKita == "Guard") {

            console.log("Halo " + peranKita + " " + namaKita + "! kamu akan membantu melindungi temanmu dari serangan werewolf!");
        }

        else if (peranKita == "Penyihir") {

            console.log("Halo " + peranKita + " " + namaKita + "! kamu dapat melihat siapa yang menjadi werewolf!");
        }

    else {

            console.log("Halo " + namaKita + "! Mohon isi Peran mu dengan benar untuk dapat memainkan!");
    }

} 

else {
            console.log("Halo~ Nama harus diisi~");
}

//Soal Switch Case
console.log("\n Soal Switch Case \n");

    //variabel
var tanggal = 1
var bulan   = 6
var tahun   = 2003

    //Switch Case
switch(bulan) {
    case 1: { console.log(tanggal + " Januari " + tahun); break;}
    case 2: { console.log(tanggal + " Februari " + tahun); break;}
    case 3: { console.log(tanggal + " Maret " + tahun); break;}
    case 4: { console.log(tanggal + " April " + tahun); break;}
    case 5: { console.log(tanggal + " Mei " + tahun); break;}
    case 6: { console.log(tanggal + " Juni " + tahun); break;}
    case 7: { console.log(tanggal + " Juli " + tahun); break;}
    case 8: { console.log(tanggal + " Agustus " + tahun); break;}
    case 9: { console.log(tanggal + " September " + tahun); break;}
    case 10: { console.log(tanggal + " Oktober " + tahun); break;}
    case 11: { console.log(tanggal + " November " + tahun); break;}
    case 12: { console.log(tanggal + " Desember " + tahun); break;}
    default: { console.log("Maaf, Bulan di Kalender Masehi hanya ada 12.")}
}

//Uwais Naufal Kusuma