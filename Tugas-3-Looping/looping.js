//Soal Nomor 1
console.log("\n Soal Nomor 1 \n");

    //Looping While
var while1 = 2 ;
var while2 = 20;

console.log(" Looping Pertama");
while(while1 <= 20){
    console.log(while1 + " - I Love Coding");
    while1 = while1 + 2;
}

console.log("\n Looping Kedua");
while (while2 >= 2) {
    console.log(while2 + " - I will become a Mobile Developer!");
    while2 = while2 - 2;
}

//Soal Nomor 2
console.log("\n Soal Nomor 2 \n");

    //Looping For

for (i = 1; i <= 20; i++) {
    if(i%2==0){
        console.log(i + " - Berkualitas");
    }
    
    else if(i%3==0){
        console.log(i + " - I Love Coding");
    }

    else{
        console.log(i + " - Santai");
    }
}

//Soal Nomor 3
console.log("\n Soal Nomor 3 \n");

    //Persegi Panjang
console.log("-Persegi Panjang-");
 var deret = 1;
 var pagar = "########";

 while(deret <= 4){
     console.log(pagar);
     deret++;
 }

 //Soal Nomor 4
 console.log("\n Soal Nomor 4 \n");

console.log("-Segitiga Siku-siku-");

 var tangga = "#";

    //Tangga Segitiga Siku2
 for (segitiga = 1; segitiga < 8; segitiga++) {
     console.log(tangga);
     tangga+="#";
 }

//Soal Nomor 5
console.log("\n Soal Nomor 5 \n");

console.log("-Papan Catur-");

var catur = "";
for (turun = 1; turun < 9; turun++) {

    if(turun%2==0){

        for (samping = 1; samping < 9; samping++) {
            if(samping%2==0){
                catur+=" ";
            } else{
                catur+="#";
            }
        
        }
        catur+="\n";
    }
    else {
        for (samping = 1; samping < 9; samping++) {
            if(samping%2==0){
                catur+="#";
            } else{
                catur+=" ";
            }
        
        }
        catur+="\n";
    }
}
console.log(catur);

//Uwais Naufal Kusuma