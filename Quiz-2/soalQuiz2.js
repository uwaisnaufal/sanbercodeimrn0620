/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

console.log("\n Soal Nomor 1")
console.log("============\n")

class Score {
    constructor(subject, points, email){
    this.subject = subject
    this.points = points
    this.email = email
    }

    get Subject() {return this.subject}
    get Points() {return this.points}
    get Email() {return this.email}
    
    average() {
        if(this.points.length > 1){
            let Mean = 0

            this.points.forEach(Jumlah => {
                Mean += Jumlah
            })

            Mean = Mean / this.points.length
            return Mean
        }
        else {
            return "Butuh lebih dari 2 Poin untuk mencari Mean"
        }
    }
}

let HasilNilai1 = new Score("Olahraga", [70, 90, 60, 80], "uwaisnaufal@gmail.com")
let HasilNilai2 = new Score("Bahasa Inggris", 100, "uwaisnaufal@gmail.com")

console.log(HasilNilai1.average())
console.log(HasilNilai2.average())


/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/
console.log("\n Soal Nomor 2")
console.log("============\n")

const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
  ]
  
  function viewScores(data, subject) {
    const HasilArr = []
    let i = 0

    data.forEach(nilai => {
        if(i > 0){
            let email = nilai[0], points
            if (subject === "quiz-1")
                points = nilai[1]
            
            else if (subject === "quiz-2") 
                points = nilai[2]
            
            else if (subject === "quiz-3") 
                points = nilai[3]
        
            let HasilObj = new Score(subject, points, email)
            HasilArr.push(HasilObj)
        }

        i++
        
    });

    console.log(HasilArr)

  }
  
  // TEST CASE
  viewScores(data, "quiz-1")
  viewScores(data, "quiz-2")
  viewScores(data, "quiz-3")

  /*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/
console.log("\n Soal Nomor 3")
console.log("============\n")

function recapScores(data) {

    let i = 0
    data.forEach(Nilai => {
        if(i > 0){
            let Mean = (Nilai[1] + Nilai[2] + Nilai[3]) /3
            let Lulus;

            if(Mean > 90) Lulus = "Honour"
            else if(Mean > 80) Lulus = "Graduate"
            else if(Mean > 70) Lulus = "Participant"
            
            console.log(`${i}. Email: ${Nilai[0]}`)
            console.log(`Rata-rata: ${Mean.toFixed(1) % 1 ? Mean.toFixed(1) : Mean.toFixed(0)}`);
            console.log(`Predikat: ${Lulus} \n`)
        }

        i++

    });
  }
  
  recapScores(data);