//Soal Nomor 1
console.log("\n Soal Nomor 1 ")
console.log("=========== \n")

//Release 0
class Animal {
    constructor(name, legs = 4, cold_blooded = false) {
      this.name = name;  
      this.legs = legs;
      this.cold_blooded = cold_blooded;
    }
}

var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

  //Release 1
console.log("===========")

class Ape extends Animal{

    constructor(name, legs = 2){
    super(name, legs)
    }

    yell(){
        console.log("Auooo!!")
    }
}
class Frog extends Animal{

    constructor(name){
    super(name)
    }

    jump(){
        console.log("Hop Hop!")
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

//Soal Nomor 2
console.log("\n Soal Nomor 2 ")
console.log("=========== \n")

class Clock{
    constructor({ template }){
    this._template = template
    }

    render(){
        var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = this._template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
    }

    stop() {
        clearInterval(timer)
    }
    start(){
        this.render()
        this._timer = setInterval(()=>{
            this.render()}, 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 