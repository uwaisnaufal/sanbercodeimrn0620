//Soal Nomor 1
console.log("\n Soal Nomor 1")
console.log("============== \n")

function arrayToObject(arr) {

    var now = new Date()
    var thisYear = now.getFullYear()
    var i=1
    arr.forEach(function bio(dataDiri) {
        
        dataDiri = {
            firstName: dataDiri[0],
            lastName: dataDiri[1],
            gender: dataDiri[2],
            age: dataDiri[3]
        }
        if (dataDiri.age <= thisYear) {
            dataDiri.age = thisYear-dataDiri.age
        }
        else {
           dataDiri.age = "Invalid Birth Year!"
        }
        console.log(`${i}. ${dataDiri.firstName.concat(" " + dataDiri.lastName)}: `)
        console.log(dataDiri)
        i++
    });

}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
console.log("====================\n")
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

arrayToObject([])

//Soal Nomor 2
console.log("\n Soal Nomor 2")
console.log("============== \n")

function shoppingTime(memberID, money){

    if(!memberID){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    else if(money < 50000){
        return "Mohon maaf, uang tidak cukup"
    }

    var barang = [
        ["Sepatu Stacattu", 1500000],
        ["Baju Zoro", 500000],
        ["Baju H&N", 250000],
        ["Sweater Uniklooh", 175000],
        ["Casing Handphone", 50000]
    ]

    objBelanja = {}

        objBelanja.memberid = memberID,
        objBelanja.money = money    

    var itemList = []
        for (i = 0; i < barang.length; i++) {
            if(money >= barang[i][1]){
                itemList.push(barang[i][0])
                money-=barang[i][1]
            }
        }
    
        objBelanja.listPurchase = itemList
        objBelanja.changeMoney = money

    return objBelanja
}

console.log(shoppingTime('1820RzKrnWn08', 2475000))
console.log("============")
console.log(shoppingTime('82Ku8Ma742', 170000))
console.log("============")
console.log(shoppingTime('', 2475000)); 
console.log("============")
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log("============")
console.log(shoppingTime()); 

//Soal Nomor 3
console.log("\n Soal Nomor 3")
console.log("============== \n")

function naikAngkot(listPenumpang){
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var arrOfObj = []

    listPenumpang.forEach(penumpang => {
        harga = 0
        angkotObj = {}
        angkotObj.penumpang = penumpang[0]
        angkotObj.naikDari = penumpang[1]
        angkotObj.tujuan = penumpang[2]

        for(i=0; i < rute.length; i++){
            if(penumpang[1] === rute[i] && penumpang[1] !== penumpang[2]){
                harga+= 2000
                penumpang[1] = rute[i+1]
            }
        }
        angkotObj.bayar = harga

        arrOfObj.push(angkotObj)
    });
    return arrOfObj
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]))
console.log(naikAngkot([]))